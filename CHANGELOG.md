[![Yii UI](https://www.yii-ui.com/logos/logo-yii-ui-readme.jpg)](https://www.yii-ui.com/) Yii UI - Yii2 Flag icon css - Widget
================================================
Change Log
----------

| Version | Date         | By     |
| ------- | ------------ | ------ |
| 1.0.X   | XX XXXX XXXX | XXXXXX |

- under development

------------------------

| Version | Date         | By     |
| ------- | ------------ | ------ |
| 1.0.1   | 22 July 2019 | cmoeke |

- Bug: Removed validation of country code length because there are flag names which are not an ISO 3166-1 alpha-2 code (cmoeke)
